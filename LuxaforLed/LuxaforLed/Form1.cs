﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LuxaforSharp;
using HidLibrary;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.Net;
using System.Speech.Synthesis;

namespace LuxaforLed
{
    public partial class Luxafor : Form
    {
        public Luxafor()
        {
            InitializeComponent();

            Synth = new SpeechSynthesizer();

            // Configure the audio output. 
            Synth.SetOutputToDefaultAudioDevice();

            
            IDeviceList list = new DeviceList();
            list.Scan();
            if (list.Count() == 0)
                return;

            device = list.First();
            RunButton.Enabled = false;
            Run(true);
        }

        private static Outlook.Items Appointments;
        private Object lockObj = new object();
        private static IDevice device;

        private Thread threadCheckCalendar;
        private Thread threadUpdateLed;
        private Thread threadChangeColor;
        private Thread threadScraperStatus;
        private Thread threadEngineStatus;

        private SpeechSynthesizer Synth;
        private AppointmentStatus currentStatus;
        private string currentAppointmentSubject;
        private LedMode currentMode;
        private int randomColorMode;
        private bool dismissed = false;

        private enum AppointmentStatus { InMeeting, Free, MeetingIn5Minutes, MeetingIn15Minutes, HugMeeting, Busy }
        private enum LedMode { FreeBusy, Random, ServerStatus }
        private void Run_Click(object sender, EventArgs e)
        {
            RunButton.Enabled = false;
            BusyButton.Enabled = true;
            ChangeColorButton.Enabled = true;
            RainBowButton.Enabled = true;
            StatusButton.Enabled = true;
            Run(true);
        }

        private void Run(bool isFree)
        {
            threadChangeColor?.Abort();
            threadScraperStatus?.Abort();
            threadEngineStatus?.Abort();

            currentMode = LedMode.FreeBusy;

            //device.Blink(LedTarget.All, new LuxaforSharp.Color(255, 0, 0), 15, 20);
            //device.SetColor(LedTarget.All, isFree ? new LuxaforSharp.Color(0, 255, 0) : new LuxaforSharp.Color(255, 0, 0));
            if (isFree)
            {
                device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 128, 0));
                currentStatus = AppointmentStatus.Free;
            }
            else
            {
                device.SetColor(LedTarget.All, new LuxaforSharp.Color(255, 0, 0));
                currentStatus = AppointmentStatus.Busy;
            }

            threadCheckCalendar = new Thread(new ThreadStart(GetAppointmentsInRange));
            threadCheckCalendar.Start();

            threadUpdateLed = new Thread(new ThreadStart(UpdateLed));
            threadUpdateLed.Start();
        }
        private void UpdateLed()
        {
            while (true)
            {
                AppointmentStatus status = currentStatus;
                lock (lockObj)
                {
                    if (Appointments != null)
                    {
                        bool hasAppointment = false;
                        foreach (Outlook.AppointmentItem appoinment in Appointments)
                        {
                            if (appoinment.MeetingStatus == Outlook.OlMeetingStatus.olMeetingCanceled)
                                continue;

                            if (DateTime.Now.AddMinutes(15) >= appoinment.Start && DateTime.Now < appoinment.Start)
                            {
                                hasAppointment = true;
                                if (!String.IsNullOrEmpty(appoinment.Subject) && appoinment.Subject.ToLower().StartsWith("hug"))
                                    status = AppointmentStatus.HugMeeting;
                                else
                                {
                                    if (DateTime.Now.AddMinutes(5) >= appoinment.Start)
                                        status = AppointmentStatus.MeetingIn5Minutes;
                                    else
                                        status = AppointmentStatus.MeetingIn15Minutes;
                                }

                                if (String.IsNullOrEmpty(currentAppointmentSubject))
                                    currentAppointmentSubject = appoinment.Subject;
                                else
                                {
                                    if (currentAppointmentSubject != appoinment.Subject)
                                    {
                                        currentAppointmentSubject = appoinment.Subject;
                                        dismissed = false;
                                    }
                                }
                            }
                            else if (DateTime.Now >= appoinment.Start && DateTime.Now <= appoinment.End)
                            {
                                status = AppointmentStatus.InMeeting;
                                hasAppointment = true;
                            }
                        }

                        if (!hasAppointment)
                            status = BusyButton.Enabled ? AppointmentStatus.Free : AppointmentStatus.Busy;

                        currentStatus = status;
                    }
                    else
                    {
                        currentStatus = BusyButton.Enabled ? AppointmentStatus.Free : AppointmentStatus.Busy;
                    }
                }
                switch (currentStatus)
                {
                    case AppointmentStatus.Free:
                        string color = ConfigurationManager.AppSettings["Color"];
                        if (!String.IsNullOrEmpty(color))
                        {
                            switch (color.ToLower())
                            {
                                case "blue":
                                    device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 0, 255));
                                    break;
                                case "yellow":
                                    device.SetColor(LedTarget.All, new LuxaforSharp.Color(255, 255, 0));
                                    break;
                            }
                        }
                        else
                            device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 128, 0));
                        break;
                    case AppointmentStatus.Busy:
                        device.SetColor(LedTarget.All, new LuxaforSharp.Color(255, 0, 0)); break;
                    case AppointmentStatus.InMeeting:
                        device.SetColor(LedTarget.All, new LuxaforSharp.Color(255, 0, 0)); break;
                    case AppointmentStatus.MeetingIn5Minutes:
                        device.Blink(LedTarget.All, new LuxaforSharp.Color(255, 0, 0), 2, 25);
                        break;
                    case AppointmentStatus.MeetingIn15Minutes:
                        device.Blink(LedTarget.All, new LuxaforSharp.Color(255, 0, 0), 5, 10);
                        break;
                    case AppointmentStatus.HugMeeting:
                        device.Blink(LedTarget.AllBackSide, new LuxaforSharp.Color(255, 0, 255), 5, 5);
                        device.Wave(WaveType.OverlappingLong, new LuxaforSharp.Color(255, 0, 255), 3, 10);
                        break;
                }

                Thread.Sleep(2000);
            }
        }

        private void GetAppointmentsInRange()
        {
            while (true)
            {
                
                lock (lockObj)
                {
                    Outlook.Application oApp;
                    oApp = new Outlook.Application();
                    Outlook.Folder folder =
                        oApp.Session.GetDefaultFolder(
                        Outlook.OlDefaultFolders.olFolderCalendar)
                        as Outlook.Folder;

                    DateTime startTime = DateTime.Now.Date;
                    DateTime endTime = DateTime.Now.AddDays(1);

                    string filter = "[Start] >= '"
                        + startTime.ToString("g")
                        + "' AND [End] <= '"
                        + endTime.ToString("g") + "'";
                    Debug.WriteLine(filter);
                    try
                    {
                        Outlook.Items calItems = folder.Items;
                        calItems.IncludeRecurrences = true;
                        calItems.Sort("[Start]", Type.Missing);
                        Outlook.Items restrictItems = calItems.Restrict(filter);
                        if (restrictItems.Count > 0)
                        {
                            Appointments = restrictItems;
                        }
                    }
                    catch { }
                }
                Thread.Sleep(2000);
            }
        }

        private void ChangeColor_Click(object sender, EventArgs e)
        {
            RunButton.Enabled = true;
            BusyButton.Enabled = true;
            RainBowButton.Enabled = true;
            ChangeColorButton.Enabled = false;
            StatusButton.Enabled = true;

            randomColorMode = 0;

            RandomColor(randomColorMode);
        }

        private void RandomColor(int mode)
        {
            //mode
            // 0 - random
            // 1 - rainbow

            threadCheckCalendar?.Abort();
            threadUpdateLed?.Abort();
            threadScraperStatus?.Abort();
            threadEngineStatus?.Abort();

            currentMode = LedMode.Random;

            threadChangeColor = new Thread(new ThreadStart(UpdateColor));
            threadChangeColor.Start();
        }

        private void UpdateColor()
        {
            while (true)
            {
                switch (randomColorMode)
                {
                    case 0:
                        Random rnd = new Random();
                        Byte[] b = new Byte[3];
                        rnd.NextBytes(b);
                        device.Wave(WaveType.OverlappingLong, new LuxaforSharp.Color(b[0], b[1], b[2]), 10, 10);
                        Thread.Sleep(1000);
                        break;
                    case 1:
                        device.CarryOutPattern(PatternType.RainbowWave, 2, 10000);
                        Thread.Sleep(9500);
                        break;
                }
            }
        }

        private void SetBusy_Click(object sender, EventArgs e)
        {
            RunButton.Enabled = true;
            BusyButton.Enabled = false;
            ChangeColorButton.Enabled = true;
            RainBowButton.Enabled = true;
            StatusButton.Enabled = true;

            Run(false);
        }

        private void RainBowButton_Click(object sender, EventArgs e)
        {
            RunButton.Enabled = true;
            BusyButton.Enabled = true;
            ChangeColorButton.Enabled = true;
            RainBowButton.Enabled = false;
            StatusButton.Enabled = true;

            randomColorMode = 1;

            RandomColor(randomColorMode);
        }

        private void CheckStatus(object sender, EventArgs e)
        {
            RunButton.Enabled = true;
            BusyButton.Enabled = true;
            ChangeColorButton.Enabled = true;
            RainBowButton.Enabled = true;
            StatusButton.Enabled = false;

            threadCheckCalendar?.Abort();
            threadUpdateLed?.Abort();
            threadChangeColor?.Abort();

            currentMode = LedMode.ServerStatus;
            device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 128, 0));

            threadScraperStatus = new Thread(new ThreadStart(CheckScraper));
            threadScraperStatus.Start();

            threadEngineStatus = new Thread(new ThreadStart(CheckEngine));
            threadEngineStatus.Start();
        }

        private int ScraperFailureCount;
        private void CheckScraper()
        {
            while (true)
            {
                UpdateLedAlert();
                try
                {
                    //HttpWebRequest webRequest = (HttpWebRequest)WebRequest
                    //                       .Create("http://mmprodmastersc:10000/SkyscraperService.asmx");
                    //webRequest.AllowAutoRedirect = false;

                    //HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
                    //HttpStatusCode statusCode = response.StatusCode;
                    HttpStatusCode statusCode = GetHeaders("http://mmprodmastersc:10000");
                    //var statusCode = HttpStatusCode.OK;
                    switch (statusCode)
                    {
                        case HttpStatusCode.OK:
                            ScraperFailureCount = 0;
                            device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 128, 0));
                            break;
                        default:
                            ScraperFailureCount++;
                            break;
                    }
                }
                catch (Exception e)
                {
                    ScraperFailureCount++;
                }

                Thread.Sleep(2000);
            }
        }

        private Object lockObj2 = new object();
        private void UpdateLedAlert()
        {
            lock (lockObj2)
            {
                if (EngineFailureCount >= 10 && ScraperFailureCount >= 10)
                {
                    device.Blink(LedTarget.All, new LuxaforSharp.Color(255, 0, 0), 2, 50);
                    Synth.Speak("Master Engine and Scraper are not working");
                }
                else if (ScraperFailureCount >= 10)
                {
                    device.Blink(LedTarget.All, new LuxaforSharp.Color(0, 17, 255), 4, 100);
                    Synth.Speak("Master Scraper is not working");
                }
                else if (EngineFailureCount >= 10)
                {
                    device.Blink(LedTarget.All, new LuxaforSharp.Color(0, 255, 128), 4, 100);
                    Synth.Speak("Master Engine is not working");
                }
                else
                    device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 128, 0));
            }
        }

        private int EngineFailureCount;
        private void CheckEngine()
        {
            while (true)
            {
                UpdateLedAlert();
                try
                {
                    HttpStatusCode statusCode = GetHeaders("http://mmprodmasteren:9000");
                    //var statusCode = HttpStatusCode.OK;
                    switch (statusCode)
                    {
                        case HttpStatusCode.OK:
                            EngineFailureCount = 0;
                            device.SetColor(LedTarget.All, new LuxaforSharp.Color(0, 128, 0));
                            break;
                        default:
                            EngineFailureCount++;
                            break;
                    }
                }
                catch (Exception e)
                {
                    EngineFailureCount++;
                }

                Thread.Sleep(2000);
            }
        }

        private HttpStatusCode GetHeaders(string url)
        {
            HttpStatusCode result = default(HttpStatusCode);

            var request = HttpWebRequest.Create(url);
            request.Method = "HEAD";
            using (var response = request.GetResponse() as HttpWebResponse)
            {
                if (response != null)
                {
                    result = response.StatusCode;
                    response.Close();
                }
            }

            return result;
        }
    }


}
