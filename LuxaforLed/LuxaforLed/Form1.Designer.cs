﻿namespace LuxaforLed
{
    partial class Luxafor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Luxafor));
            this.RunButton = new System.Windows.Forms.Button();
            this.ChangeColorButton = new System.Windows.Forms.Button();
            this.BusyButton = new System.Windows.Forms.Button();
            this.RainBowButton = new System.Windows.Forms.Button();
            this.StatusButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RunButton
            // 
            this.RunButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RunButton.Location = new System.Drawing.Point(65, 31);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(140, 23);
            this.RunButton.TabIndex = 0;
            this.RunButton.Text = "Set Free";
            this.RunButton.UseCompatibleTextRendering = true;
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.Run_Click);
            // 
            // ChangeColorButton
            // 
            this.ChangeColorButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ChangeColorButton.Location = new System.Drawing.Point(65, 119);
            this.ChangeColorButton.Name = "ChangeColorButton";
            this.ChangeColorButton.Size = new System.Drawing.Size(140, 23);
            this.ChangeColorButton.TabIndex = 1;
            this.ChangeColorButton.Text = "Random Color";
            this.ChangeColorButton.UseCompatibleTextRendering = true;
            this.ChangeColorButton.UseVisualStyleBackColor = true;
            this.ChangeColorButton.Click += new System.EventHandler(this.ChangeColor_Click);
            // 
            // BusyButton
            // 
            this.BusyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BusyButton.Location = new System.Drawing.Point(65, 74);
            this.BusyButton.Name = "BusyButton";
            this.BusyButton.Size = new System.Drawing.Size(140, 23);
            this.BusyButton.TabIndex = 2;
            this.BusyButton.Text = "Set Busy";
            this.BusyButton.UseCompatibleTextRendering = true;
            this.BusyButton.UseVisualStyleBackColor = true;
            this.BusyButton.Click += new System.EventHandler(this.SetBusy_Click);
            // 
            // RainBowButton
            // 
            this.RainBowButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RainBowButton.Location = new System.Drawing.Point(65, 163);
            this.RainBowButton.Name = "RainBowButton";
            this.RainBowButton.Size = new System.Drawing.Size(140, 23);
            this.RainBowButton.TabIndex = 3;
            this.RainBowButton.Text = "RainBow";
            this.RainBowButton.UseCompatibleTextRendering = true;
            this.RainBowButton.UseVisualStyleBackColor = true;
            this.RainBowButton.Click += new System.EventHandler(this.RainBowButton_Click);
            // 
            // StatusButton
            // 
            this.StatusButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.StatusButton.Location = new System.Drawing.Point(65, 206);
            this.StatusButton.Name = "StatusButton";
            this.StatusButton.Size = new System.Drawing.Size(140, 23);
            this.StatusButton.TabIndex = 4;
            this.StatusButton.Text = "Server Status";
            this.StatusButton.UseCompatibleTextRendering = true;
            this.StatusButton.UseVisualStyleBackColor = true;
            this.StatusButton.Click += new System.EventHandler(this.CheckStatus);
            // 
            // Luxafor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.StatusButton);
            this.Controls.Add(this.RainBowButton);
            this.Controls.Add(this.BusyButton);
            this.Controls.Add(this.ChangeColorButton);
            this.Controls.Add(this.RunButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 300);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "Luxafor";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Luxafor";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.Button ChangeColorButton;
        private System.Windows.Forms.Button BusyButton;
        private System.Windows.Forms.Button RainBowButton;
        private System.Windows.Forms.Button StatusButton;
    }
}

